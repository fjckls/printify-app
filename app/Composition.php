<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Composition extends Model
{
    protected $fillable = [ 'thumb', 'full_image', 'settings'];


    public function setSettingsAttribute( $value )
    {
      $this->attributes['settings'] = json_encode($value);
    }


    public function getSettingsAttribute($value)
    {
        return json_decode( $value );
    }


    
    public function getThumbAttribute($value)
    {
        return url('uploads/' . $value);
    }


    public function getFullImageAttribute($value)
    {
        return url( 'uploads/' . $value );
    }
    
    

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ManageImages@frontPage');
Route::get('dashboard', 'ManageImages@dashboard');
Route::get('dashboard/print/{id}', 'ManageImages@generatePrint');




Route::group(['middleware' => 'cors'], function () {
   
    Route::options('api/handleImages', function(){
   		return \Response::make('cors ok', 200);
   	});
   	Route::options('api/getOptions', function(){
   		return \Response::make('cors ok', 200);
   	});
   	
   	Route::post('api/handleImages', 'Api@handleImages');
   	Route::post('api/getOptions', 'Api@getOptions');
  


});




//Route::resource('api/hadnleImages', 'Api@hadnleImages');

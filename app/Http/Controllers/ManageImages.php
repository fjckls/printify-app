<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Composition;


class ManageImages extends Controller
{
    
    public function frontPage()
    {
    	return view('home')->with(['js'=>'/js/bundle.js']);
    }




    public function dashboard()
    {
    	$compositions = Composition::latest()->get();

    	return view('dashboard',compact('compositions'));
    }	


    public function generatePrint( $id )
    {
    	

    	$composition = Composition::findOrFail($id);
    	
    	$settings = $composition->settings;
    	$path 		= public_path('uploads/' . $composition->getOriginal('full_image') );

    	
    	$image = \Image::make( $path );
    	$image->resize( $settings->scale * $image->width(), null, function ($constraint) {
			    $constraint->aspectRatio();
			});


    	
    	$canvas = \Image::canvas(env('BIG_CANVAS_W'), env('BIG_CANVAS_H'));
    	    	

    	$halfW =  $image->width() / 2 ;
    	$halfH =  $image->height() / 2 ;

    	$xpos = (( ($settings->x - env('CANVAS_OFFSET_L') ) / env('SMALL_CANVAS_W') ) * env('BIG_CANVAS_W')) - $halfW;
    	$ypos = (( ($settings->y - env('CANVAS_OFFSET_T') ) / env('SMALL_CANVAS_H')) * env('BIG_CANVAS_H')) - $halfH;


    	$data = (string) $canvas->insert($image, 'top-left', (int)$xpos, (int)$ypos)->encode('data-url') ;

    	
    	return view('printFile', compact('data'));
    	
    }	

}

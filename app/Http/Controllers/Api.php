<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Composition;

class Api extends Controller
{


		public function __construct()
		{
			

		}




		private function createComposition( $upload )
		{
				$now 	= time();
    		$extension = $upload->guessExtension() == 'jpeg' ? '.jpg' : '.'.$upload->guessExtension();
      	$origFilename = $upload->getClientOriginalName();
      	$finalFilename = str_slug( pathinfo($origFilename, PATHINFO_FILENAME) );
    		
          	
      	// Thumb
    		$thumb = $finalFilename .'_thumb_' . $now . $extension;
				\Image::make($upload)->fit(env('THUMB_W'))->save( public_path('uploads/' . $thumb)) ;

				
				// Full size image
				$original = $finalFilename .'_original_' . $now . $extension;
				\Image::make($upload)->save( public_path('uploads/' . $original)) ;

				
				Composition::create([
					'thumb' 			=> $thumb,
					'full_image' 	=> $original,
					'settings'		=> [
							'x' => env('SMALL_CANVAS_W') / 2 + env('CANVAS_OFFSET_L'), 
							'y' => env('SMALL_CANVAS_H') / 2 + env('CANVAS_OFFSET_T'), 'scale' => 1],
				]);
		}





		public function getOptions(Request $request)
		{
			$composition = Composition::findOrFail($request->input('id'));
			return json_encode($composition->settings);
		}





    public function handleImages(Request $request)
    {


    	// Check for incoming image

    	$upload = $request->file('file');

    	if( $upload ){

    		$data = $this->createComposition($upload);
    		
    	}


    
    	// Check if composition is being updated

    	if( $request->input('update') &&  $request->input('settings')){
    		
    		$composition = Composition::findOrFail($request->input('update'));
				$composition->settings = $request->input('settings');
				$composition->update();

    	}

    	$compositions = Composition::latest()->get();
    	return $compositions;


 

    }

    
}

@extends ('master')

@section('content')
	
	@if( !$compositions->isEmpty())
  	

  	@foreach( $compositions as $composition)
	  	<div class='col-sm-6 col-md-3 thumbs-list'>
	      <div class='thumbnail'>
	         <img src='{{ $composition->thumb }}'>
	      </div>
	      
	      <div class='caption'>
	         
	         <p><code>{{ json_encode($composition->settings) }}</code></p>
	         
	         <p>
	            <a href='/dashboard/print/{{ $composition->id }}' target='_blank' class='btn btn-primary full-w' role='button' style='width:100%'>
	               View print file
	            </a> 
	         </p>
	         
	      </div>
	   </div>
   @endforeach


  @endif

@stop